import * as ActionDoctor from '../Store/DoctorAction';
import * as ActionTemplate from '../../Template/Store/TemplateAction';
import * as SelectorDoctor from '../Selector/DoctorSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';

function* fetchDoctorListProcess() {
  try {
    const { data } = yield call(
      axios.get,
      `${process.env.REACT_APP_DOCTOR_URL}/Doctor/inquiry/0/1000`
    );
    yield put(ActionDoctor.fetchDoctorListFinished(data.data));
    console.log(data);
  } catch (error) {
    console.log(error);
  }
}

function* deleteDoctorProcces() {
  try {
    const id = yield select(SelectorDoctor.idSelector());
    yield call(
      axios.delete,
      `${process.env.REACT_APP_DOCTOR_URL}/Doctor/${id}`
    );
    yield put(ActionDoctor.fetchDoctorListRequested());
  } catch (error) {
    console.log(error);
  }
}

function* submitDoctorProcces() {
  try {
    const name = yield select(SelectorDoctor.formNameSelector());
    const poli = yield select(SelectorDoctor.formPoliSelector());
    const address = yield select(SelectorDoctor.formaAdressSelector());
    const day = yield select(SelectorDoctor.formDaySelector());
    const time = yield select(SelectorDoctor.formTimeSelector());
    const contact = yield select(SelectorDoctor.formContactSelector());

    yield call(
      axios.post,
      `${process.env.REACT_APP_DOCTOR_URL}/Doctor/AddDoctor`,
      {
        name,
        poli,
        address,
        day,
        time,
        contact,
      }
    );

    yield put(ActionDoctor.fetchDoctorListRequested());
    yield put(ActionTemplate.openModal('Doctor'));
  } catch (error) {
    console.log(error);
  }
}

function* updateDoctorProcces() {
  try {
    const id = yield select(SelectorDoctor.idSelector());
    const name = yield select(SelectorDoctor.formNameSelector());
    const poli = yield select(SelectorDoctor.formPoliSelector());
    const address = yield select(SelectorDoctor.formaAdressSelector());
    const day = yield select(SelectorDoctor.formDaySelector());
    const time = yield select(SelectorDoctor.formTimeSelector());
    const contact = yield select(SelectorDoctor.formContactSelector());

    yield call(
      axios.put,
      `${process.env.REACT_APP_DOCTOR_URL}/Doctor/UpdateDoctor`,
      {
        id,
        name,
        poli,
        address,
        day,
        time,
        contact,
      }
    );

    yield put(ActionDoctor.fetchDoctorListRequested());
    yield put(ActionTemplate.openModal('Doctor'));
  } catch (error) {
    console.log(error);
  }
}

export function* fetchDoctorListAction() {
  yield takeLatest('FETCH_DOCTOR_LIST_REQUESTED', fetchDoctorListProcess);
}
export function* deleteDoctorAction() {
  yield takeLatest('DELETE_DOCTOR_REQUESTED', deleteDoctorProcces);
}

export function* submitDoctorAction() {
  yield takeLatest('SUBMIT_DOCTOR_REQUESTED', submitDoctorProcces);
}

export function* updateDoctorAction() {
  yield takeLatest('UPDATE_DOCTOR_REQUESTED', updateDoctorProcces);
}