import { createSelector } from 'reselect';

const doctorState = (state: any) => state.DoctorState;
const doctorFormState = (state: any) => state.form.doctorForm.values;

export const listSelector = () =>
  createSelector(doctorState, state => state.list);
export const idSelector = () =>
  createSelector(doctorState, state => state.selectedIdDoctor);
export const selectedDoctorSelector = () =>
  createSelector(doctorState, state => state.selectedDoctor);
export const modalActionSelector = () =>
  createSelector(doctorState, state => state.modalAction);

//SELECTOR FORM
export const formNameSelector = () =>
  createSelector(doctorFormState, state => state.name);
  export const formPoliSelector = () =>
  createSelector(doctorFormState, state => state.poli);
export const formaAdressSelector = () =>
  createSelector(doctorFormState, state => state.address);
export const formDaySelector = () =>
  createSelector(doctorFormState, state => state.day);
export const formTimeSelector = () =>
  createSelector(doctorFormState, state => state.time);
export const formContactSelector = () =>
  createSelector(doctorFormState, state => state.contact);