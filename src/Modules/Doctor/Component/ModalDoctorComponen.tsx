import { Button, Form, Modal } from 'antd';
import { Field, reduxForm } from 'redux-form';

import { InputText } from '../../../Assets/Components/CInputText';
import { Poli } from '../../../Assets/Components/Enum';
import React from 'react';
import { SelectPoli } from '../../../Assets/Components/CSelectPoli';
import validate from '../Validation/DoctorValidation';

function ModalDoctorComponen(props) {
  const {
    handleCancelModal,
    modalDoctorIsShow,
    handleOnSubmit,
    invalid,
  } = props;
 
  return (
    <Modal
      title="Data Doctor"
      visible={modalDoctorIsShow}
      onCancel={handleCancelModal}
      footer={null}
    >
      <Form onFinish={handleOnSubmit} layout="vertical">
      <Field
          name="name"
          component={InputText}
          label="Name"
          placeholder="Input name here"
        />

        <Field
          name="poli"
          component={SelectPoli}
          label="Poli"
          data={Poli}/>
        
        <Field
          name="address"
          component={InputText}
          label="Address"
          placeholder="Input address here"
        />
        <Field
          name="day"
          component={InputText}
          label="Day"
          placeholder="Input day here"
        />

<Field
          name="time"
          component={InputText}
          label="Time"
          placeholder="Input time here"
        />

<Field
          name="contact"
          component={InputText}
          label="Contact"
          placeholder="Input contact here"
        />
        <Form.Item label=" " colon={false}>
          <Button type="primary" htmlType="submit" disabled={invalid}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default reduxForm({
  form: 'doctorForm',
  validate,
  enableReinitialize: true,
})(ModalDoctorComponen); //HOC
