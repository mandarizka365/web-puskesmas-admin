import { Button } from 'antd';
import ModalDoctorContainer from '../Container/ModalDoctorContainer';
import { PlusOutlined } from '@ant-design/icons/lib/icons';
import React from 'react';
import Table from '../../../Assets/Components/CTable';

export default function DoctorComponen(props) {
  const { colums, data, handleAddDoctor } = props;
  return (
    <div>
      <ModalDoctorContainer {...props} />
      <Button
        icon={<PlusOutlined />}
        className="addbutton"
        type="primary"
          onClick={handleAddDoctor}
        />
      <Table columns={colums} data={data} />
    </div>
  );
}
