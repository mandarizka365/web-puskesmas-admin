import * as DoctorAction from '../Store/DoctorAction';
import * as SelectorDoctor from '../Selector/DoctorSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import { Button } from 'antd';
import DoctorComponen from '../Component/DoctorComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function DoctorContainer(props) {
  const { actionDoctor, listData, actionTemplate } = props;

  const renderAction = row => {
    const handleUpdate = () => {
      actionDoctor.changeModalAction('update');
      actionDoctor.setDoctorId(row.row.original.id);
      console.log('doctor Id : ', row.row.original.id);
      
      actionDoctor.setDoctorDetail(row.row.original);
      actionTemplate.openModal('Doctor');
    };
    const handleDelete = () => {
      actionDoctor.setDoctorId(row.row.original.id);
      actionDoctor.deleteDoctorRequested(row.row.original.id);
    };
    return (
      <React.Fragment>
         <Button
         icon={<EditOutlined />}
          className="btnActionTable"
          type="primary"
          onClick={handleUpdate}
        />
        
        <Button
        icon={<DeleteOutlined />}
          className="btnActionTable"
          type="primary"
          danger
          onClick={handleDelete}
        />

      </React.Fragment>
    );
  };
  const colums = [
    {
      Header: 'Doctor Name',
      accessor: 'name',
    },
    {
      Header: 'Poli',
      accessor: 'poli',
    },
    {
      Header: 'Address',
      accessor: 'address',
    },
    {
      Header: 'Day',
      accessor: 'day',
    },
    {
      Header: 'Time',
      accessor: 'time',
    },
    {
      Header: 'Contact',
      accessor: 'contact',
    },
    {
      Header: 'Action',
      Cell: renderAction,
    },
  ];

  useEffect(() => {
    actionDoctor.fetchDoctorListRequested();
  }, [actionDoctor]);

  const handleAddDoctor = () => {
    actionTemplate.openModal('Doctor');
    actionDoctor.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('Doctor');
    actionDoctor.changeModalAction('register');
  };

  return (
    <DoctorComponen
      handleAddDoctor={handleAddDoctor}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorDoctor.listSelector(),
  modalDoctorIsShow: SelectorTemplate.modalDoctorSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionDoctor: bindActionCreators(DoctorAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(DoctorContainer);
