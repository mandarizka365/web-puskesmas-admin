import { Action } from 'redux';

// dr template action

export interface IFetchDoctor extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchDoctorListRequested functin utk nembak API

export function fetchDoctorListRequested() {
  return {
    type: 'FETCH_DOCTOR_LIST_REQUESTED',
  };
}

export function setDoctorId(id: string): ISetId {
  return {
    type: 'SET_DOCTOR_ID',
    id,
  };
}

export function updateDoctorRequested() {
  return {
    type: 'UPDATE_DOCTOR_REQUESTED',
  };
}

export function deleteDoctorRequested(id: string): ISetId {
  return {
    type: 'DELETE_DOCTOR_REQUESTED',
    id,
  };
}

export function setDoctorDetail(data: object): ISetDetail {
  return {
    type: 'SET_DOCTOR_DETAIL',
    data,
  };
}

export function submitDoctorRequested() {
  return {
    type: 'SUBMIT_DOCTOR_REQUESTED',
  };
}

export function fetchDoctorListFinished(data: object): IFetchDoctor {
  return {
    type: 'FETCH_DOCTOR_LIST_FINISHED',
    data,
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_DOCTOR_ACTION',
    typeAction,
  };
}
