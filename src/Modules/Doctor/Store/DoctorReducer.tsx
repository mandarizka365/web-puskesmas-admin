import {
  IFetchDoctor,
  IModalAction,
  ISetDetail,
  ISetId,
} from './DoctorAction';

import { Action } from 'redux';

export const initialState: any = {
  list: [],
  selectedDoctor: {},
  selectedIdDoctor: '',
  modalAction: 'register',
};

export default function DoctorReducer(state = initialState, action: Action) {
  if (!action) return state;
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'FETCH_DOCTOR_LIST_FINISHED': {
      const fetchDoctor = action as IFetchDoctor;
      newState.list = fetchDoctor.data;
      return { ...newState };
    }
    case 'CHANGE_MODAL_DOCTOR_ACTION': {
      const modalAction = action as IModalAction;
      newState.modalAction = modalAction.typeAction;
      return { ...newState };
    }
    case 'SET_DOCTOR_ID': {
      const setId = action as ISetId;
      newState.selectedIdDoctor = setId.id;
      return { ...newState };
    }
    case 'SET_DOCTOR_DETAIL': {
      const setDetail = action as ISetDetail;
      newState.selectedDoctor = setDetail.data;
      return { ...newState };
    }
  }
  return state;
}
