export default function DoctorValidation(values) {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Name Required!';
  }
  if (!values.poli) {
    errors.poli = 'Poli Required!';
  }
  if (!values.address) {
    errors.address = 'Address Required!';
  }
  if (!values.day) {
    errors.day = 'Schedule Day Required!';
  }
  if (!values.time) {
    errors.time = 'Schedule Time Required!';
  }
  if (!values.contact) {
    errors.contact = 'Contact Required!';
  }
  return errors;
}
