export default function OldValidation(values) {
    const errors: any = {};
    if (!values.nik) {
      errors.nik = 'NIK Required!';
    } else if (values.nik.length < 16) {
      errors.nik = 'Minimal NIK / Card Number 16 characters';
    }
    if (!values.registration) {
      errors.registration = 'Date registration Required!';
    }
    if (!values.poli) {
      errors.poli = 'Poli Required!';
    }
    if (!values.jenisBayar) {
      errors.jenisBayar = 'Payment Type Required!';
    }return errors;
  }
  