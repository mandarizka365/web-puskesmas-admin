import { Button, Form, Modal } from 'antd';
import { Buy, Poli } from '../../../Assets/Components/Enum';
import { DatePickers, InputText } from '../../../Assets/Components/CInputText';
import { Field, reduxForm } from 'redux-form';

import React from 'react';
import { SelectBuy } from '../../../Assets/Components/CSelectBuy';
import { SelectPoli } from '../../../Assets/Components/CSelectPoli';
import validate from '../Validation/OldValidation';

function ModalOldComponen(props) {
  const {
    handleCancelModal,
    modalOldIsShow,
    handleOnSubmit,
    invalid,
  } = props;
 
  return (
    <Modal
      title="Data Old"
      visible={modalOldIsShow}
      onCancel={handleCancelModal}
      footer={null}
    >
      <Form onFinish={handleOnSubmit} layout="vertical">
      <Field
          name="nik"
          component={InputText}
          label="NIK / No Kartu"
          placeholder="Input NIK / No Kartu here"
        />

        <Field
          name="registration"
          component={DatePickers}
          label="Date Registration"
        />
        
        <Field
          name="poli"
          component={SelectPoli}
          label="Tujuan Poli"
          data={Poli}/>

        <Field
          name="jenisBayar"
          component={SelectBuy}
          label="Jenis Bayar"
          data={Buy}
        />

        <Form.Item label=" " colon={false}>
          <Button type="primary" htmlType="submit" disabled={invalid}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default reduxForm({
  form: 'oldForm',
  validate,
  enableReinitialize: true,
})(ModalOldComponen); //HOC
