import { Button } from 'antd';
import ModalOldContainer from '../Container/ModalOldContainer';
import { PlusOutlined } from '@ant-design/icons/lib/icons';
import React from 'react';
import Table from '../../../Assets/Components/CTable';

export default function OldComponen(props) {
  const { colums, data, handleAddOld} = props;
  return (
    <div>
      <ModalOldContainer {...props} />
      <Button
        icon={<PlusOutlined />}
        className="addbutton"
        type="primary"
          onClick={handleAddOld}
      />
      <Table columns={colums} data={data}  />
    </div>
  );
}