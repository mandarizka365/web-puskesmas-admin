import {
    IFetchOld,
    IModalAction,
    ISetDetail,
    ISetId,
} from './OldAction';

import { Action } from 'redux';

export const initialState: any = {
    list: [],
    selectedOld: {},
    selectedIdOld: '',
    modalAction: 'register',
  };
  
  export default function OldReducer(state = initialState, action: Action) {
    if (!action) return state;
    const newState = Object.assign({}, state);
    switch (action.type) {
      case 'FETCH_OLD_LIST_FINISHED': {
        const fetchOld = action as IFetchOld;
        newState.list = fetchOld.data;
        return { ...newState };
      }
      case 'CHANGE_MODAL_OLD_ACTION': {
        const modalAction = action as IModalAction;
        newState.modalAction = modalAction.typeAction;
        return { ...newState };
      }
      case 'SET_OLD_ID': {
        const setId = action as ISetId;
        newState.selectedIdOld = setId.id;
        return { ...newState };
      }
      case 'SET_OLD_DETAIL': {
        const setDetail = action as ISetDetail;
        newState.selectedOld = setDetail.data;
        return { ...newState };
      }
    }
    return state;
  }
  