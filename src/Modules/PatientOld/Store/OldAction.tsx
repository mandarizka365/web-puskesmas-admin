import { Action } from 'redux';

// dr template action

export interface IFetchOld extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchOldListRequested functin utk nembak API

export function fetchOldListRequested() {
  return {
    type: 'FETCH_OLD_LIST_REQUESTED',
  };
}

export function setOldId(id: string): ISetId {
  return {
    type: 'SET_OLD_ID',
    id,
  };
}

export function updateOldRequested() {
  return {
    type: 'UPDATE_OLD_REQUESTED',
  };
}

export function deleteOldRequested(id: string): ISetId {
  return {
    type: 'DELETE_OLD_REQUESTED',
    id,
  };
}

export function setOldDetail(data: object): ISetDetail {
  return {
    type: 'SET_OLD_DETAIL',
    data,
  };
}

export function submitOldRequested() {
  return {
    type: 'SUBMIT_OLD_REQUESTED',
  };
}

export function fetchOldListFinished(data: object): IFetchOld {
  return {
    type: 'FETCH_OLD_LIST_FINISHED',
    data,
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_OLD_ACTION',
    typeAction,
  };
}
