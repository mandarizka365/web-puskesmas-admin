import * as ActionOld from '../Store/OldAction';
import * as ActionTemplate from '../../Template/Store/TemplateAction';
import * as SelectorOld from '../Selector/OldSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';

function* fetchOldListProcess() {
  try {
    const { data } = yield call(
      axios.get,
      `${process.env.REACT_APP_OLD_URL}/Old/inquiry/0/1000`
    );
    yield put(ActionOld.fetchOldListFinished(data.data));
    console.log(data);
  } catch (error) {
    console.log(error);
  }
}

function* deleteOldProcces() {
  try {
    const id = yield select(SelectorOld.idSelector());
    yield call(
      axios.delete,
      `${process.env.REACT_APP_OLD_URL}/Old/${id}`
    );
    yield put(ActionOld.fetchOldListRequested());
  } catch (error) {
    console.log(error);
  }
}

function* submitOldProcces() {
  try {
    const nik = yield select(SelectorOld.formNIKSelector());
    const registration = yield select(SelectorOld.formRegistrationSelector());
    const poli = yield select(SelectorOld.formPoliSelector());
    const jenisBayar = yield select(SelectorOld.formJenisBayarSelector());

    yield call(
      axios.post,
      `${process.env.REACT_APP_OLD_URL}/Old/AddOld`,
      {
        nik,
        registration,
        poli,
        jenisBayar,
      }
    );

    yield put(ActionOld.fetchOldListRequested());
    yield put(ActionTemplate.openModal('Old'));
  } catch (error) {
    console.log(error);
  }
}

function* updateOldProcces() {
  try {
    const id = yield select(SelectorOld.idSelector());
    const nik = yield select(SelectorOld.formNIKSelector());
    const registration = yield select(SelectorOld.formRegistrationSelector());
    const poli = yield select(SelectorOld.formPoliSelector());
    const jenisBayar = yield select(SelectorOld.formJenisBayarSelector());

    yield call(
      axios.put,
      `${process.env.REACT_APP_OLD_URL}/Old/UpdateOld`,
      {
        id,
        nik,
        registration,
        poli,
        jenisBayar,
      }
    );

    yield put(ActionOld.fetchOldListRequested());
    yield put(ActionTemplate.openModal('Old'));
  } catch (error) {
    console.log(error);
  }
}

export function* fetchOldListAction() {
  yield takeLatest('FETCH_OLD_LIST_REQUESTED', fetchOldListProcess);
}
export function* deleteOldAction() {
  yield takeLatest('DELETE_OLD_REQUESTED', deleteOldProcces);
}

export function* submitOldAction() {
  yield takeLatest('SUBMIT_OLD_REQUESTED', submitOldProcces);
}

export function* updateOldAction() {
  yield takeLatest('UPDATE_OLD_REQUESTED', updateOldProcces);
}