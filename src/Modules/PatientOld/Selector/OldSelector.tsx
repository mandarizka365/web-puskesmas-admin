import { createSelector } from 'reselect';

const oldState = (state: any) => state.OldState;
const oldFormState = (state: any) => state.form.oldForm.values;

export const listSelector = () =>
  createSelector(oldState, state => state.list);
export const idSelector = () =>
  createSelector(oldState, state => state.selectedIdOld);
export const selectedOldSelector = () =>
  createSelector(oldState, state => state.selectedOld);
export const modalActionSelector = () =>
  createSelector(oldState, state => state.modalAction);

//SELECTOR FORM
export const formNIKSelector = () =>
  createSelector(oldFormState, state => state.nik);
export const formRegistrationSelector = () =>
  createSelector(oldFormState, state => state.registration);
export const formPoliSelector = () =>
  createSelector(oldFormState, state => state.poli);
export const formJenisBayarSelector = () =>
  createSelector(oldFormState, state => state.jenisBayar);