import * as OldAction from '../Store/OldAction';
import * as SelectorOld from '../Selector/OldSelector';

import { bindActionCreators, compose } from 'redux';

import ModalOldComponen from '../Component/ModalOldComponen';
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function ModalOldContainer(props) {
  const { actionOld, oldDetail, modalAction } = props;
  const handleOnSubmit = () => {
    if (modalAction === 'register') {
      actionOld.submitOldRequested();
    } else {
      actionOld.updateOldRequested();
    }
  };
  const initialValues: any = {};
  if (Object.keys(oldDetail).length > 0) {
    const {
        nik,
        registration,
        poli,
        jenisBayar,
    } = oldDetail;
    initialValues.nik = nik;
    initialValues.registration = registration;
    initialValues.poli = poli;
    initialValues.jenisBayar = jenisBayar;
  }
  return (
    <ModalOldComponen
      initialValues={initialValues}
      handleOnSubmit={handleOnSubmit}
      {...props}
    />
  );
}
const mapStateToProps = createStructuredSelector({
  oldDetail: SelectorOld.selectedOldSelector(),
  modalAction: SelectorOld.modalActionSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionOld: bindActionCreators(OldAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(ModalOldContainer);
