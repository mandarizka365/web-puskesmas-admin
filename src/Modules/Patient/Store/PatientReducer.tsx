import {
  IFetchPatient,
  IModalAction,
  ISetDetail,
  ISetId,
} from './PatientAction';

import { Action } from 'redux';

export const initialState: any = {
  list: [],
  selectedPatient: {},
  selectedIdPatient: '',
  modalAction: 'register',
};

export default function PatientReducer(state = initialState, action: Action) {
  if (!action) return state;
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'FETCH_PATIENT_LIST_FINISHED': {
      const fetchPatient = action as IFetchPatient;
      newState.list = fetchPatient.data;
      return { ...newState };
    }
    case 'CHANGE_MODAL_PATIENT_ACTION': {
      const modalAction = action as IModalAction;
      newState.modalAction = modalAction.typeAction;
      return { ...newState };
    }
    case 'SET_PATIENT_ID': {
      const setId = action as ISetId;
      newState.selectedIdPatient = setId.id;
      return { ...newState };
    }
    case 'SET_PATIENT_DETAIL': {
      const setDetail = action as ISetDetail;
      newState.selectedPatient = setDetail.data;
      return { ...newState };
    }
  }
  return state;
}
