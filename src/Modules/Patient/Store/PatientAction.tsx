import { Action } from 'redux';

// dr template action

export interface IFetchPatient extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchPatientListRequested functin utk nembak API

export function fetchPatientListRequested() {
  return {
    type: 'FETCH_PATIENT_LIST_REQUESTED',
  };
}

export function setPatientId(id: string): ISetId {
  return {
    type: 'SET_PATIENT_ID',
    id,
  };
}

export function updatePatientRequested() {
  return {
    type: 'UPDATE_PATIENT_REQUESTED',
  };
}

export function deletePatientRequested(id: string): ISetId {
  return {
    type: 'DELETE_PATIENT_REQUESTED',
    id,
  };
}

export function setPatientDetail(data: object): ISetDetail {
  return {
    type: 'SET_PATIENT_DETAIL',
    data,
  };
}

export function submitPatientRequested() {
  return {
    type: 'SUBMIT_PATIENT_REQUESTED',
  };
}

export function fetchPatientListFinished(data: object): IFetchPatient {
  return {
    type: 'FETCH_PATIENT_LIST_FINISHED',
    data,
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_PATIENT_ACTION',
    typeAction,
  };
}
