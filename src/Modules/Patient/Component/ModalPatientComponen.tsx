import { Button, Form, Modal } from 'antd';
import { DatePickers, InputText } from '../../../Assets/Components/CInputText';
import { Field, reduxForm } from 'redux-form';
import { Gender, Poli } from '../../../Assets/Components/Enum';

import React from 'react';
import { SelectGender } from '../../../Assets/Components/CSelectGender';
import { SelectPoli } from '../../../Assets/Components/CSelectPoli';
import validate from '../Validation/PatientValidation';

function ModalPatientComponen(props) {
  const {
    handleCancelModal,
    modalPatientIsShow,
    handleOnSubmit,
    invalid,
  } = props;
 
  return (
    <Modal
      title="Data Patient"
      visible={modalPatientIsShow}
      onCancel={handleCancelModal}
      footer={null}
    >
      <Form onFinish={handleOnSubmit} layout="horizontal">
      <Field
          name="name"
          component={InputText}
          label="Name"
          placeholder="Input name here"
        />

        <Field
          name="address"
          component={InputText}
          label="Address"
          placeholder="Input address here"
        />
        
        <Field
          name="dateOfBirth"
          component={DatePickers}
          label="Date Of Birth"
        />

        <Field
          name="gender"
          component={SelectGender}
          label="Gender"
          data={Gender}
        />

        <Field
          name="nameParent"
          component={InputText}
          label="Name Parent"
          placeholder="Input nameParent here"
        />

        <Field
          name="telephone"
          component={InputText}
          label="Telephone"
          placeholder="Input telephone here"
        />

<Field
          name="nik"
          component={InputText}
          label="NIK"
          placeholder="Input nik here"
        />

<Field
          name="noBPJS"
          component={InputText}
          label="No BPJS"
          placeholder="Input noBPJS here"
        />

<Field
          name="registration"
          component={DatePickers}
          label="Tanggal Pendaftaran"
        />

<Field
          name="poli"
          component={SelectPoli}
          label="Tujuan Poli"
          data={Poli}/>

        <Form.Item label=" " colon={false}>
          <Button type="primary" htmlType="submit" disabled={invalid}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default reduxForm({
  form: 'patientForm',
  validate,
  enableReinitialize: true,
})(ModalPatientComponen); //HOC
