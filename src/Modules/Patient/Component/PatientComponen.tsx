import { Button } from 'antd';
import ModalPatientContainer from '../Container/ModalPatientContainer';
import { PlusOutlined } from '@ant-design/icons/lib/icons';
import React from 'react';
import Table from '../../../Assets/Components/CTable';

export default function PatientComponen(props) {
  const { colums, data, handleAddPatient} = props;
  return (
    <div>
      <ModalPatientContainer {...props} />
      <Button
        icon={<PlusOutlined />}
        className="addbutton"
        type="primary"
          onClick={handleAddPatient}
      />
      <Table columns={colums} data={data}  />
    </div>
  );
}