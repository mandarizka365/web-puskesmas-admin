export default function PatientValidation(values) {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Name Required!';
  } 
  if (!values.address) {
    errors.address = 'Address Required!';
  }
  if (!values.dateOfBirth) {
    errors.dateOfBirth = 'Date of Birth Required!';
  }
  if (!values.gender) {
    errors.gender = 'gender Required!';
  }
  if (!values.nameParent) {
    errors.nameParent = 'Parent Name Required!';
  }
  if (!values.telephone) {
    errors.telephone = 'Telephone Required!';
  }
  if (!values.nik) {
    errors.nik = 'NIK Required!';
  } else if (values.nik.length < 16) {
    errors.nik = 'Minimal NIK characters';
  }
  if (!values.registration) {
    errors.registration = 'Date Registration Required!';
  }
  if (!values.poli) {
    errors.poli = 'Date Registration Required!';
  }
  return errors;
}
