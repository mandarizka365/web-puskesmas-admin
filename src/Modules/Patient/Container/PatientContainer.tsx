import * as PatientAction from '../Store/PatientAction';
import * as SelectorPatient from '../Selector/PatientSelector';
import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import { Button } from 'antd';
import PatientComponen from '../Component/PatientComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';

function PatientContainer(props) {
  const { actionPatient, listData, actionTemplate } = props;

  const renderAction = row => {
    const handleUpdate = () => {
      actionPatient.changeModalAction('update');
      actionPatient.setPatientId(row.row.original.id);      
      actionPatient.setPatientDetail(row.row.original);
      actionTemplate.openModal('Patient');
    };
    const handleDelete = () => {
      actionPatient.setPatientId(row.row.original.id);
      actionPatient.deletePatientRequested(row.row.original.id);
    };
    

    return (
      <React.Fragment>
         <Button
         icon={<EditOutlined />}
          className="btnActionTable"
          type="primary"
          onClick={handleUpdate}
        />
        
        <Button
        icon={<DeleteOutlined />}
          className="btnActionTable"
          type="primary"
          danger
          onClick={handleDelete}
        />

      </React.Fragment>
    );
  };

  const registration = row => {
    console.log(row);
    
    return (
      moment(row.registration).format("DD MMMM YYYY")
      
    )
  };

  const colums = [
    {
      Header: 'Name',
      accessor: 'name',
    },
    {
      Header: 'Registration Date',
      accessor: row => registration(row),
    },
    {
      Header: 'Address',
      accessor: 'address',
    },
    {
      Header: 'Gender',
      accessor: 'gender',
    },
    {
      Header: 'NIK',
      accessor: 'nik',
    },
    {
      Header: 'NO BPJS',
      accessor: 'noBPJS',
    },
    {
      Header: 'Action',
      Cell: renderAction,
    },
  ];

  useEffect(() => {
    actionPatient.fetchPatientListRequested();
  }, [actionPatient]);

  const handleAddPatient = () => {
    actionTemplate.openModal('Patient');
    actionPatient.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('Patient');
    actionPatient.changeModalAction('register');
  };

  return (
    <PatientComponen
      handleAddPatient={handleAddPatient}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorPatient.listSelector(),
  modalPatientIsShow: SelectorTemplate.modalPatientSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionPatient: bindActionCreators(PatientAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(PatientContainer);
