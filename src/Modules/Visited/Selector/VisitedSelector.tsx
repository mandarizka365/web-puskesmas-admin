import { createSelector } from 'reselect';

const visitedState = (state: any) => state.VisitedState;
const visitedFormState = (state: any) => state.form.visitedForm.values;

export const listSelector = () =>
  createSelector(visitedState, state => state.list);
export const idSelector = () =>
  createSelector(visitedState, state => state.selectedIdVisited);
export const selectedVisitedSelector = () =>
  createSelector(visitedState, state => state.selectedVisited);
export const modalActionSelector = () =>
  createSelector(visitedState, state => state.modalAction);

//SELECTOR FORM
export const formaNameSelector = () =>
  createSelector(visitedFormState, state => state.name);
export const formAddressSelector = () =>
  createSelector(visitedFormState, state => state.address);
export const formRegistrationSelector = () =>
  createSelector(visitedFormState, state => state.registration);
export const formNameDoctorSelector = () =>
  createSelector(visitedFormState, state => state.nameDoctor);
export const formPoliSelector = () =>
  createSelector(visitedFormState, state => state.poli);
export const formTelephoneSelector = () =>
  createSelector(visitedFormState, state => state.telephone);
