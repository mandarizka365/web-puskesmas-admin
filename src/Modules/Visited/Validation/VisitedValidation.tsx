export default function VisitedValidation(values) {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Name Required!';
  } else if (values.name.length < 3) {
    errors.name = 'Minimal Name 3 characters';
  }
  if (!values.address) {
    errors.address = 'Address Required!';
  }
  if (!values.registration) {
    errors.registration = 'Date Registration Required!';
  }
  if (!values.nameDoctor) {
    errors.nameDoctor = 'Name Doctor Required!';
  }
  if (!values.poli) {
    errors.poli = 'Poli Required!';
  }
  if (!values.telephone) {
    errors.telephone = 'Telephone Required!';
  }
  return errors;
}
