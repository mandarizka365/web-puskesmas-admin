import * as ActionTemplate from '../../Template/Store/TemplateAction';
import * as ActionVisited from '../Store/VisitedAction';
import * as SelectorVisited from '../Selector/VisitedSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';

function* fetchVisitedListProcess() {
  try {
    const { data } = yield call(
      axios.get,
      `${process.env.REACT_APP_VISITED_URL}/Visited/inquiry/0/1000`
    );
    yield put(ActionVisited.fetchVisitedListFinished(data.data));
    console.log(data);
  } catch (error) {
    console.log(error);
  }
}

function* deleteVisitedProcces() {
  try {
    const id = yield select(SelectorVisited.idSelector());
    yield call(
      axios.delete,
      `${process.env.REACT_APP_VISITED_URL}/Visited/${id}`,
    );
    yield put(ActionVisited.fetchVisitedListRequested());
  } catch (error) {
    console.log(error);
  }
}

function* submitVisitedProcces() {
  try {
    const name = yield select(SelectorVisited.formaNameSelector());
    const address = yield select(SelectorVisited.formAddressSelector());
    const registration = yield select(
      SelectorVisited.formRegistrationSelector()
    );
    const nameDoctor = yield select(SelectorVisited.formNameDoctorSelector());
    const poli = yield select(
      SelectorVisited.formPoliSelector()
    );
    const telephone = yield select(SelectorVisited.formTelephoneSelector());

    yield call(
      axios.post,
      `${process.env.REACT_APP_VISITED_URL}/Visited/AddVisited`,
      {
        name,
        address,
        registration,
      nameDoctor,
      poli,
      telephone,
      }
    );

    yield put(ActionVisited.fetchVisitedListRequested());
    yield put(ActionTemplate.openModal('Visited'));
  } catch (error) {
    console.log(error);
  }
}

function* updateVisitedProcces() {
  try {
    const id = yield select(SelectorVisited.idSelector());
    const name = yield select(SelectorVisited.formaNameSelector());
    const address = yield select(SelectorVisited.formAddressSelector());
    const registration = yield select(
      SelectorVisited.formRegistrationSelector()
    );
    const nameDoctor = yield select(SelectorVisited.formNameDoctorSelector());
    const poli = yield select(
      SelectorVisited.formPoliSelector()
    );
    const telephone = yield select(SelectorVisited.formTelephoneSelector());

    yield call(
      axios.put,
      `${process.env.REACT_APP_VISITED_URL}/Visited/UpdateVisited`,
      {
        id,
        name,
        address,
        registration,
      nameDoctor,
      poli,
      telephone,
      }
    );

    yield put(ActionVisited.fetchVisitedListRequested());
    yield put(ActionTemplate.openModal('Visited'));
  } catch (error) {
    console.log(error);
  }
}

export function* fetchVisitedListAction() {
  yield takeLatest('FETCH_VISITED_LIST_REQUESTED', fetchVisitedListProcess);
}
export function* deleteVisitedAction() {
  yield takeLatest('DELETE_VISITED_REQUESTED', deleteVisitedProcces);
}

export function* submitVisitedAction() {
  yield takeLatest('SUBMIT_VISITED_REQUESTED', submitVisitedProcces);
}

export function* updateVisitedAction() {
  yield takeLatest('UPDATE_VISITED_REQUESTED', updateVisitedProcces);
}
