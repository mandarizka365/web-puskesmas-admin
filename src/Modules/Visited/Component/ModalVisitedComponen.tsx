import { Button, Form, Modal } from 'antd';
import { DatePickers, InputText } from '../../../Assets/Components/CInputText';
import { Field, reduxForm } from 'redux-form';

import { Poli } from '../../../Assets/Components/Enum';
import React from 'react';
import { SelectPoli } from '../../../Assets/Components/CSelectPoli';
import validate from '../Validation/VisitedValidation';

function ModalVisitedComponen(props) {
  const {
    handleCancelModal,
    modalVisitedIsShow,
    handleOnSubmit,
    invalid,
  } = props;
  return (
    <Modal
      title="Visited Data"
      visible={modalVisitedIsShow}
      onCancel={handleCancelModal}
      footer={null}
    >
      <Form onFinish={handleOnSubmit} layout="vertical">
        <Field
          name="name"
          component={InputText}
          label="Name"
          placeholder="Input name here"
        />

        <Field
          name="address"
          component={InputText}
          label="Address"
          placeholder="Input address here"
        />

        <Field
          name="registration"
          label="Registration"
          component={DatePickers}
          
        />

        <Field
          name="nameDoctor"
          component={InputText}
          label="Doctor Name"
          placeholder="Input Name Doctor here"
        />

<Field
          name="poli"
          component={SelectPoli}
          label="Poli"
          data={Poli}/>

        <Field
          name="telephone"
          component={InputText}
          label="telephone"
          placeholder="Input telephone here"
        />
        <Form.Item label=" " colon={false}>
          <Button type="primary" htmlType="submit" disabled={invalid}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default reduxForm({
  form: 'visitedForm',
  validate,
  enableReinitialize: true,
})(ModalVisitedComponen); //HOC
