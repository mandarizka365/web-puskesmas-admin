// import { Button } from 'antd';

import ModalVisitedContainer from '../Container/ModalVisitedContainer';
import React from 'react';
import Table from '../../../Assets/Components/CTable';

// import { PlusOutlined } from '@ant-design/icons/lib/icons';



export default function VisitedComponen(props) {
  const { colums, data} = props;
  return (
    <div>
      <ModalVisitedContainer {...props} />
        {/* <Button
          icon={<PlusOutlined />}
          className="addbutton"
            type="primary"
            onClick={handleAddVisited}
        /> */}
      <Table columns={colums} data={data} />
    </div>
  );
}
