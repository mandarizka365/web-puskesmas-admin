import { Action } from 'redux';

// dr template action

export interface IFetchVisited extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchVisitedListRequested functin utk nembak API

export function fetchVisitedListRequested() {
  return {
    type: 'FETCH_VISITED_LIST_REQUESTED',
  };
}

export function setVisitedId(id: string): ISetId {
  return {
    type: 'SET_VISITED_ID',
    id,
  };
}

export function updateVisitedRequested() {
  return {
    type: 'UPDATE_VISITED_REQUESTED',
  };
}

export function deleteVisitedRequested(id: string): ISetId {
  return {
    type: 'DELETE_VISITED_REQUESTED',
    id,
  };
}

export function setVisitedDetail(data: object): ISetDetail {
  return {
    type: 'SET_VISITED_DETAIL',
    data,
  };
}

export function submitVisitedRequested() {
  return {
    type: 'SUBMIT_VISITED_REQUESTED',
  };
}

export function fetchVisitedListFinished(data: object): IFetchVisited {
  return {
    type: 'FETCH_VISITED_LIST_FINISHED',
    data,
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_VISITED_ACTION',
    typeAction,
  };
}
