import {
  IFetchVisited,
  IModalAction,
  ISetDetail,
  ISetId,
} from './VisitedAction';

import { Action } from 'redux';

export const initialState: any = {
  list: [],
  selectedVisited: {},
  selectedIdVisited: '',
  modalAction: 'register',
};

export default function VisitedReducer(state = initialState, action: Action) {
  if (!action) return state;
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'FETCH_VISITED_LIST_FINISHED': {
      const fetchVisited = action as IFetchVisited;
      newState.list = fetchVisited.data;
      return { ...newState };
    }
    case 'CHANGE_MODAL_VISITED_ACTION': {
      const modalAction = action as IModalAction;
      newState.modalAction = modalAction.typeAction;
      return { ...newState };
    }
    case 'SET_VISITED_ID': {
      const setId = action as ISetId;
      newState.selectedIdVisited = setId.id;
      return { ...newState };
    }
    case 'SET_VISITED_DETAIL': {
      const setDetail = action as ISetDetail;
      newState.selectedVisited = setDetail.data;
      return { ...newState };
    }
  }
  return state;
}
