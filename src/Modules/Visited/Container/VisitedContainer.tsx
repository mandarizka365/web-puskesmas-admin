import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as SelectorVisited from '../Selector/VisitedSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';
import * as VisitedAction from '../Store/VisitedAction';

import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import { Button } from 'antd';
import VisitedComponen from '../Component/VisitedComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import moment from 'moment';

function VisitedContainer(props) {
  const { actionVisited, listData, actionTemplate } = props;

  const renderAction = row => {
    const handleUpdate = () => {
      actionVisited.changeModalAction('update');
      actionVisited.setVisitedId(row.row.original.id);
      actionVisited.setVisitedDetail(row.row.original);
      actionTemplate.openModal('Visited');
    };

    const handleDelete = () => {
      actionVisited.setVisitedId(row.row.original.id);
      actionVisited.deleteVisitedRequested(row.row.original.id);
    };
    return (
      <React.Fragment>
        <Button
         icon={<EditOutlined />}
          className="btnActionTable"
          type="primary"
          onClick={handleUpdate}
        />
        
        <Button
        icon={<DeleteOutlined />}
          className="btnActionTable"
          type="primary"
          danger
          onClick={handleDelete}
        />
      </React.Fragment>
    );
  };

  const registration = row => {
    console.log(row);
    
    return (
      moment(row.registration).format("DD MMMM YYYY")
    )
  };

  const colums = [
    {
      Header: 'Patient Name',
      accessor: 'name',
    },
    {
      Header: 'Registration',
      accessor: row => registration(row),
    },
    {
      Header: 'Doctor Name',
      accessor: 'nameDoctor',
    },
    {
      Header: 'Poli',
      accessor: 'poli',
    },
    {
      Header: 'Action',
      Cell: renderAction,
    },
  ];

  useEffect(() => {
    actionVisited.fetchVisitedListRequested();
  }, [actionVisited]);

  const handleAddVisited = () => {
    actionTemplate.openModal('Visited');
    actionVisited.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('Visited');
    actionVisited.changeModalAction('register');
  };

  return (
    <VisitedComponen
      handleAddVisited={handleAddVisited}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorVisited.listSelector(),
  modalVisitedIsShow: SelectorTemplate.modalVisitedSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionVisited: bindActionCreators(VisitedAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(VisitedContainer);
