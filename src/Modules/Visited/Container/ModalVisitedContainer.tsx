import * as SelectorVisited from '../Selector/VisitedSelector';
import * as VisitedAction from '../Store/VisitedAction';

import { bindActionCreators, compose } from 'redux';

import ModalVisitedComponen from '../Component/ModalVisitedComponen';
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function ModalVisitedContainer(props) {
  const { actionVisited, visitedDetail, modalAction } = props;
  const handleOnSubmit = () => {
    if (modalAction === 'register') {
      actionVisited.submitVisitedRequested();
    } else {
      actionVisited.updateVisitedRequested();
    }
    
  };
  const initialValues: any = {};
  if (Object.keys(visitedDetail).length > 0) {
    const {
      name,
      address,
      registration,
      nameDoctor,
      poli,
      telephone,
    } = visitedDetail;
    initialValues.name = name;
    initialValues.address = address;
    initialValues.registration = registration;
    initialValues.nameDoctor = nameDoctor;
    initialValues.poli = poli;
    initialValues.telephone = telephone;
  }
  return (
    <ModalVisitedComponen
      initialValues={initialValues}
      handleOnSubmit={handleOnSubmit}
      {...props}
    />
  );
}
const mapStateToProps = createStructuredSelector({
  visitedDetail: SelectorVisited.selectedVisitedSelector(),
  modalAction: SelectorVisited.modalActionSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionVisited: bindActionCreators(VisitedAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(ModalVisitedContainer);
