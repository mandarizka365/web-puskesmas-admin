import * as ActionLogin from '../Store/LoginAction';
import * as SelectorLogin from '../Selector/LoginSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';
import history from '../../App/History'

function* fetchLoginListProcess() {
  try {
    const username = yield select(SelectorLogin.formUsernameSelector());
    const password = yield select(SelectorLogin.formPasswordSelector());
    const { data } = yield call(
      axios.get,
      `${process.env.REACT_APP_LOGIN_URL}/Login/Inquiry/1/10/${username}/${password}`
    );
    yield put(ActionLogin.fetchLoginListFinished(data.data));
    console.log('data login',data.data)
    if (data.data[0].id.length > 0) {
      history.push('/admin');
    }
  } catch (error) {
    console.log(error);
  }
}

export function* fetchLoginListAction() {
    yield takeLatest('FETCH_LOGIN_LIST_REQUESTED', fetchLoginListProcess);
  }