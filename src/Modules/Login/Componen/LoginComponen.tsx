import { Button, Col, Form, Layout, Row } from 'antd';
import { Field, reduxForm } from 'redux-form';

import { InputPassword } from '../../../Assets/Components/CinputPassword';
import { InputText } from '../../../Assets/Components/CInputText';
import React from 'react';
import images from '../../App/Images'
import validate from '../Validation/LoginValidation';

const { Content } = Layout;


function LoginComponen(props) {
  const {
    handleOnSubmit,
    invalid,
  } = props;
  return (
    <Layout style={{height:"100vh"}}>
      <Content className="content1">
      
      <Row>
      <Col xs={24} sm={24} md={24} lg={12} xl={12}>
        <img src={images.gambarsatu} alt="doctor"  className="pict1"/>
      </Col>
      <Col xs={24} sm={24} md={24} lg={12} xl={12} >
      <div className="divcolom">
          <Form onFinish={handleOnSubmit} layout="horizontal">
            <Field
              name="username"
              component={InputText}
              label="Username"
              placeholder="Input username here"
            />

            <Field
              name="password"
              component={InputPassword}
              label="Password"
              placeholder="Input password here"
            />

            <Form.Item>
              <Button type="primary" htmlType="submit" disabled={invalid}>
                Login
              </Button>
              <a href="/regis" className="ahref">Registrasi akun.</a>
            </Form.Item>
          </Form>
      </div>
      </Col>
    </Row>
      </Content>
    </Layout>
      
  );
}

export default reduxForm({
  form: 'loginForm',
  validate,
  enableReinitialize: true,
})(LoginComponen); //HOC
