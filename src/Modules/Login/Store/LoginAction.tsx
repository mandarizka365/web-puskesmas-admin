import { Action } from 'redux';

// dr template action

export interface IFetchLogin extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchLoginListRequested functin utk nembak API

export function fetchLoginListRequested() {
  return {
    type: 'FETCH_LOGIN_LIST_REQUESTED',
  };
}

export function updateLoginRequested() {
  return {
    type: 'UPDATE_LOGIN_REQUESTED',
  };
}


export function setLoginId(id: string): ISetId {
  return {
    type: 'SET_LOGIN_ID',
    id,
  };
}

export function setLoginDetail(data: object): ISetDetail {
  return {
    type: 'SET_LOGIN_DETAIL',
    data,
  };
}

export function fetchLoginListFinished(data: object): IFetchLogin {
  return {
    type: 'FETCH_LOGIN_LIST_FINISHED',
    data,
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_LOGIN_ACTION',
    typeAction,
  };
}
