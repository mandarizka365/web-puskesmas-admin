import { IFetchLogin, IModalAction, ISetDetail, ISetId } from './LoginAction';

import { Action } from 'redux';

export const initialState: any = {
  list: [],
  selectedLogin: {},
  selectedIdLogin: '',
  modalAction: 'register',
};

export default function LoginReducer(state = initialState, action: Action) {
  if (!action) return state;
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'FETCH_LOGIN_LIST_FINISHED': {
      const fetchLogin = action as IFetchLogin;
      newState.list = fetchLogin.data;
      return { ...newState };
    }
    case 'CHANGE_MODAL_LOGIN_ACTION': {
      const modalAction = action as IModalAction;
      newState.modalAction = modalAction.typeAction;
      return { ...newState };
    }
    case 'SET_LOGIN_ID': {
      const setId = action as ISetId;
      newState.selectedIdLogin = setId.id;
      return { ...newState };
    }
    case 'SET_LOGIN_DETAIL': {
      const setDetail = action as ISetDetail;
      newState.selectedLogin = setDetail.data;
      return { ...newState };
    }
  }
  return state;
}
