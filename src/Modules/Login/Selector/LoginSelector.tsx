import { createSelector } from 'reselect';

const loginState = (state: any) => state.LoginState;
const loginFormState = (state: any) => state.form.loginForm.values;

export const listSelector = () =>
  createSelector(loginState, state => state.list);
export const idSelector = () =>
  createSelector(loginState, state => state.selectedIdLogin);
export const selectedLoginSelector = () =>
  createSelector(loginState, state => state.selectedLogin);
export const modalActionSelector = () =>
  createSelector(loginState, state => state.modalAction);

//SELECTOR FORM

export const formUsernameSelector = () =>
  createSelector(loginFormState, state => state.username);
export const formPasswordSelector = () =>
  createSelector(loginFormState, state => state.password);

