import * as LoginAction from '../Store/LoginAction';

import { bindActionCreators, compose } from 'redux';

import LoginComponen from '../Componen/LoginComponen'
import React from 'react'
import { connect } from 'react-redux';

function LoginContainer(props) {
    const { actionLogin } = props;
  const handleOnSubmit = () => {
    actionLogin.fetchLoginListRequested();
  };
    return <LoginComponen handleOnSubmit={handleOnSubmit}/>;
}

const mapDispatchToProps = (dispatch: any) => ({
    actionLogin: bindActionCreators(LoginAction, dispatch),
  });
const withConnect = connect(null, mapDispatchToProps);
  export default compose(withConnect)(LoginContainer);