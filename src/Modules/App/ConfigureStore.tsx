import * as _ from 'lodash';

import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import { persistReducer, persistStore } from 'redux-persist';

import DoctorState from '../Doctor/Store/DoctorReducer';
import LoginState from '../Login/Store/LoginReducer';
import OldState from '../PatientOld/Store/OldReducer'
import PatientState from '../Patient/Store/PatientReducer';
import Saga from './SagaMiddleware';
import TemplateState from '../Template/Store/TemplateReducer';
import UserState from '../User/Store/UserReducer';
import VisitedState from '../Visited/Store/VisitedReducer';
import { composeWithDevTools } from 'redux-devtools-extension';
import { connectRouter } from 'connected-react-router';
import createSagaMiddleware from 'redux-saga';
import { reducer as formReducer } from 'redux-form';
import history from './History';
import storage from 'redux-persist/lib/storage';

const combinedReducer = combineReducers({
  TemplateState,
  DoctorState,
  VisitedState,
  UserState,
  PatientState,
  OldState,
  LoginState,
  router: connectRouter(history),
  form: formReducer,
});

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['TemplateState'],
};
const persistedReducer = persistReducer(persistConfig, combinedReducer);

const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  _.has(window, '__REDUX_DEVTOOLS_EXTENSION_COMPOSE__')
    ? composeWithDevTools({ trace: true, traceLimit: 1000 })
    : compose;

const sagaMiddleware = createSagaMiddleware();
export const store = createStore(
  persistedReducer,
  composeEnhancers(applyMiddleware(sagaMiddleware))
);
export const persistor = persistStore(store);
sagaMiddleware.run(Saga);
