import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import DoctorContainer from '../Doctor/Container/DoctorContainer';
import LoginContainer from '../Login/Container/LoginContainer'
import OldContainer from '../PatientOld/Container/OldContainer';
import PatientContainer from '../Patient/Container/PatientContainer';
import RegisContainer from '../Register/Container/RegisContainer';
import UserContainer from '../User/Container/UserContainer';
import VisitedContainer from '../Visited/Container/VisitedContainer';
import WithTemplate from './WithTemplate';

export default class Navigation extends Component {
  render() {
    const doctor = WithTemplate(DoctorContainer);
    const visited = WithTemplate(VisitedContainer);
    const admin = WithTemplate(UserContainer);
    const patient = WithTemplate(PatientContainer);
    const oldPatient = WithTemplate(OldContainer);
    const regis = RegisContainer;
    const login = LoginContainer

    return (
      <Switch>
        <Route path="/" exact={true} component={login} />
        <Route path="/regis" exact={true} component={regis} />
        <Route path="/doctor" exact={true} component={doctor} />
        <Route path="/visited" exact={true} component={visited} />
        <Route path="/admin" exact={true} component={admin} />
        <Route path="/patient" exact={true} component={patient} />
        <Route path="/oldPatient" exact={true} component={oldPatient} />
      </Switch>
    );
  }
}
