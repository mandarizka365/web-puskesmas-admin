import { all, fork } from 'redux-saga/effects';
import {
  deleteDoctorAction,
  fetchDoctorListAction,
  submitDoctorAction,
  updateDoctorAction,
} from '../Doctor/Saga/DoctorSaga';
import {
  deleteOldAction,
  fetchOldListAction,
  submitOldAction,
  updateOldAction,
} 
  from '../PatientOld/Saga/OldSaga';
import {
  deletePatientAction,
  fetchPatientListAction,
  submitPatientAction,
  updatePatientAction,
} from '../Patient/Saga/PatientSaga';
import {
  deleteUserAction,
  fetchUserListAction,
  submitUserAction,
  updateUserAction,
} from '../User/Saga/UserSaga';
import {
  deleteVisitedAction,
  fetchVisitedListAction,
  submitVisitedAction,
  updateVisitedAction,
} from '../Visited/Saga/VisitedSaga';

import { fetchLoginListAction } from '../Login/Saga/LoginSaga';
import {submitRegisAction} from '../Register/Saga/RegisSaga';

export default function* () {
  yield all([
    fork(fetchUserListAction),
    fork(submitUserAction),
    fork(deleteUserAction),
    fork(updateUserAction),
    fork(submitDoctorAction),
    fork(updateDoctorAction),
    fork(deleteDoctorAction),
    fork(fetchDoctorListAction),
    fork(submitVisitedAction),
    fork(updateVisitedAction),
    fork(deleteVisitedAction),
    fork(fetchVisitedListAction),
    fork(submitPatientAction),
    fork(updatePatientAction),
    fork(deletePatientAction),
    fork(fetchPatientListAction),
    fork(submitOldAction),
    fork(deleteOldAction),
    fork(updateOldAction),
    fork(fetchOldListAction),
    fork(fetchLoginListAction),
    fork(submitRegisAction)
  ]);
}
