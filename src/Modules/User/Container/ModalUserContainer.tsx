import * as SelectorUser from '../Selector/UserSelector';
import * as UserAction from '../Store/UserAction';

import { bindActionCreators, compose } from 'redux';

import ModalUserComponen from '../Component/ModalUserComponen';
import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function ModalUserContainer(props) {
  const { actionUser, userDetail, modalAction } = props;
  const handleOnSubmit = () => {
    if (modalAction === 'register') {
      actionUser.submitUserRequested();
    } else {
      actionUser.updateUserRequested();
    }
  };
  const initialValues: any = {};
  if (Object.keys(userDetail).length > 0) {
    const {
      name,
      username,
      password,
    } = userDetail;
    initialValues.name = name;
    initialValues.username = username;
    initialValues.password = password;
  }
  return (
    <ModalUserComponen
      initialValues={initialValues}
      handleOnSubmit={handleOnSubmit}
      {...props}
    />
  );
}
const mapStateToProps = createStructuredSelector({
  userDetail: SelectorUser.selectedUserSelector(),
  modalAction: SelectorUser.modalActionSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionUser: bindActionCreators(UserAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(ModalUserContainer);
