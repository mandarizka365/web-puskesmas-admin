import * as SelectorTemplate from '../../Template/Selector/TemplateSelector';
import * as SelectorUser from '../Selector/UserSelector';
import * as TemplateAction from '../../Template/Store/TemplateAction';
import * as UserAction from '../Store/UserAction';

import { DeleteOutlined, EditOutlined } from '@ant-design/icons/lib/icons';
import React, { useEffect } from 'react';
import { bindActionCreators, compose } from 'redux';

import { Button } from 'antd';
import UserComponen from '../Component/UserComponen';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';

function UserContainer(props) {
  const { actionUser, listData, actionTemplate } = props;

  const renderAction = row => {
    const handleUpdate = () => {
      actionUser.changeModalAction('update');
      actionUser.setUserId(row.row.original.id);
      actionUser.setUserDetail(row.row.original);
      actionTemplate.openModal('User');
    };

    const handleDelete = () => {
      actionUser.setUserId(row.row.original.id);
      actionUser.deleteUserRequested(row.row.original.id);

    };
    return (
      <React.Fragment>
        <Button
         icon={<EditOutlined />}
          className="btnActionTable"
          type="primary"
          onClick={handleUpdate}
        />
        
        <Button
        icon={<DeleteOutlined />}
          className="btnActionTable"
          type="primary"
          danger
          onClick={handleDelete}
        />
      </React.Fragment>
    );
  };
  const colums = [
    {
      Header: 'Admin Name',
      accessor: 'name',
    },
    {
      Header: 'Username',
      accessor: 'username',
    },
    {
      Header: 'Action',
      Cell: renderAction,
    },
  ];

  useEffect(() => {
    actionUser.fetchUserListRequested();
  }, [actionUser]);

  const handleAddUser = () => {
    actionTemplate.openModal('User');
    actionUser.changeModalAction('register');
  };

  const handleCancelModal = () => {
    actionTemplate.openModal('User');
    actionUser.changeModalAction('register');
  };

  return (
    <UserComponen
      handleAddUser={handleAddUser}
      handleCancelModal={handleCancelModal}
      colums={colums}
      data={listData}
      {...props}
    />
  );
}

const mapStateToProps = createStructuredSelector({
  listData: SelectorUser.listSelector(),
  modalUserIsShow: SelectorTemplate.modalUserSelector(),
});

const mapDispatchToProps = (dispatch: any) => ({
  actionUser: bindActionCreators(UserAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(mapStateToProps, mapDispatchToProps);
export default compose(withConnect)(UserContainer);
