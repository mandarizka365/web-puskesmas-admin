// import { Button } from 'antd';

import ModalUserContainer from '../Container/ModalUserContainer';
import React from 'react';
import Table from '../../../Assets/Components/CTable';

// import { PlusOutlined } from '@ant-design/icons/lib/icons';



export default function UserComponen(props) {
  const { colums, data } = props;
  return (
    <div>
      <ModalUserContainer {...props} />
        {/* <Button
          icon={<PlusOutlined />}
          className="addbutton"
            type="primary"
            onClick={handleAddUser}
        /> */}
      <Table columns={colums} data={data} />
      
    </div>
  );
}
