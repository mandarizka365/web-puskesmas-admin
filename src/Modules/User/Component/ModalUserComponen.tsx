import { Button, Form, Modal } from 'antd';
import { Field, reduxForm } from 'redux-form';

import { InputPassword } from '../../../Assets/Components/CinputPassword';
import { InputText } from '../../../Assets/Components/CInputText';
import React from 'react';
import validate from '../Validation/UserValidation';

function ModalUserComponen(props) {
  const {
    handleCancelModal,
    modalUserIsShow,
    handleOnSubmit,
    invalid,
  } = props;
  return (
    <Modal
      title="Data User"
      visible={modalUserIsShow}
      onCancel={handleCancelModal}
      footer={null}
    >
      <Form onFinish={handleOnSubmit} layout="horizontal">
      <Field
          name="name"
          component={InputText}
          label="Name"
          placeholder="Input name here"
        />
        
        <Field
          name="username"
          component={InputText}
          label="Username"
          placeholder="Input username here"
        />


        <Field
          name="password"
          component={InputPassword}
          label="Password"
          placeholder="Input password here"
        />

        <Form.Item>
          <Button type="primary" htmlType="submit" disabled={invalid}>
            Submit
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}

export default reduxForm({
  form: 'userForm',
  validate,
  enableReinitialize: true,
})(ModalUserComponen); //HOC
