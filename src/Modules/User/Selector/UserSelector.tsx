import { createSelector } from 'reselect';

const userState = (state: any) => state.UserState;
const userFormState = (state: any) => state.form.userForm.values;

export const listSelector = () =>
  createSelector(userState, state => state.list);
export const idSelector = () =>
  createSelector(userState, state => state.selectedIdUser);
export const selectedUserSelector = () =>
  createSelector(userState, state => state.selectedUser);
export const modalActionSelector = () =>
  createSelector(userState, state => state.modalAction);

//SELECTOR FORM
export const formNameSelector = () =>
  createSelector(userFormState, state => state.name);
export const formUsernameSelector = () =>
  createSelector(userFormState, state => state.username);
export const formPasswordSelector = () =>
  createSelector(userFormState, state => state.password);

