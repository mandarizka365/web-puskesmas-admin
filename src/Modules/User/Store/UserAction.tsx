import { Action } from 'redux';

// dr template action

export interface IFetchUser extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchUserListRequested functin utk nembak API

export function fetchUserListRequested() {
  return {
    type: 'FETCH_USER_LIST_REQUESTED',
  };
}

export function updateUserRequested() {
  return {
    type: 'UPDATE_USER_REQUESTED',
  };
}


export function setUserId(id: string): ISetId {
  return {
    type: 'SET_USER_ID',
    id,
  };
}

export function deleteUserRequested(id: string): ISetId {
  return {
    type: 'DELETE_USER_REQUESTED',
    id,
  };
}

export function setUserDetail(data: object): ISetDetail {
  return {
    type: 'SET_USER_DETAIL',
    data,
  };
}

export function submitUserRequested() {
  return {
    type: 'SUBMIT_USER_REQUESTED',
  };
}

export function fetchUserListFinished(data: object): IFetchUser {
  return {
    type: 'FETCH_USER_LIST_FINISHED',
    data,
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_USER_ACTION',
    typeAction,
  };
}
