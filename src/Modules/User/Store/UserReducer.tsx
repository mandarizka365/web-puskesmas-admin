import { IFetchUser, IModalAction, ISetDetail, ISetId } from './UserAction';

import { Action } from 'redux';

export const initialState: any = {
  list: [],
  selectedUser: {},
  selectedIdUser: '',
  modalAction: 'register',
};

export default function UserReducer(state = initialState, action: Action) {
  if (!action) return state;
  const newState = Object.assign({}, state);
  switch (action.type) {
    case 'FETCH_USER_LIST_FINISHED': {
      const fetchUser = action as IFetchUser;
      newState.list = fetchUser.data;
      return { ...newState };
    }
    case 'CHANGE_MODAL_USER_ACTION': {
      const modalAction = action as IModalAction;
      newState.modalAction = modalAction.typeAction;
      return { ...newState };
    }
    case 'SET_USER_ID': {
      const setId = action as ISetId;
      newState.selectedIdUser = setId.id;
      return { ...newState };
    }
    case 'SET_USER_DETAIL': {
      const setDetail = action as ISetDetail;
      newState.selectedUser = setDetail.data;
      return { ...newState };
    }
  }
  return state;
}
