import * as ActionTemplate from '../../Template/Store/TemplateAction';
import * as ActionUser from '../Store/UserAction';
import * as SelectorUser from '../Selector/UserSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';

function* fetchUserListProcess() {
  try {
    const { data } = yield call(
      axios.get,
      `${process.env.REACT_APP_LOGIN_URL}/Login/Inquiry/0/1000`
    );
    yield put(ActionUser.fetchUserListFinished(data.data));
    console.log('LOGIN', data);
  } catch (error) {
    console.log(error);
  }
}

function* deleteUserProcces() {
  try {
    const id = yield select(SelectorUser.idSelector());
    yield call(axios.delete, `${process.env.REACT_APP_LOGIN_URL}/Login/${id}`);
    yield put(ActionUser.fetchUserListRequested());
  } catch (error) {
    console.log(error);
  }
}

function* submitUserProcces() {
  try {
    const name = yield select(
      SelectorUser.formNameSelector()
    );
    const username = yield select(
      SelectorUser.formUsernameSelector()
    );
    const password = yield select(SelectorUser.formPasswordSelector());

    yield call(axios.post, `${process.env.REACT_APP_LOGIN_URL}/Login/AddLogin`, {
      name,
      username,
      password,
    });

    yield put(ActionUser.fetchUserListRequested());
    yield put(ActionTemplate.openModal('User'));
  } catch (error) {
    console.log(error);
  }
}

function* updateUserProcces() {
  try {
    const id = yield select(SelectorUser.idSelector());
    const name = yield select(
      SelectorUser.formNameSelector()
    );
    const username = yield select(
      SelectorUser.formUsernameSelector()
    );
    const password = yield select(SelectorUser.formPasswordSelector());

    yield call(
      axios.put,
      `${process.env.REACT_APP_LOGIN_URL}/Login/UpdateLogin`,
      {
        id,
        name,
        username,
        password,
      }
    );

    yield put(ActionUser.fetchUserListRequested());
    yield put(ActionTemplate.openModal('User'));
  } catch (error) {
    console.log(error);
  }
}

export function* fetchUserListAction() {
  yield takeLatest('FETCH_USER_LIST_REQUESTED', fetchUserListProcess);
}
export function* deleteUserAction() {
  yield takeLatest('DELETE_USER_REQUESTED', deleteUserProcces);
}

export function* submitUserAction() {
  yield takeLatest('SUBMIT_USER_REQUESTED', submitUserProcces);
}

export function* updateUserAction() {
  yield takeLatest('UPDATE_USER_REQUESTED', updateUserProcces);
}