import { Layout, Menu } from 'antd';
import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UploadOutlined,
  UserAddOutlined,
  UserOutlined,
  UserSwitchOutlined,
} from '@ant-design/icons';
import React, { Component } from 'react'

import { Link } from 'react-router-dom';

const { Header, Content, Sider } = Layout;


interface IProps {
    handleToggleSider: any;
    siderIsOpen: boolean;
  }

export default class TemplateComponent extends Component<IProps> {
  state = {
    collapsed: false,
  };

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };
  
    render() {
        return (
          <Layout style={{height:"100vh"}}>
          <Sider className="siderItem" trigger={null} collapsible collapsed={this.state.collapsed}>
          <div className="logo">PUSKA</div>
            <Menu className="menuSatu" mode="inline" >
              <Menu.Item className="menuMenu" key="admin" icon={<UserOutlined />}>
              <Link className="linkstyle" to="/admin">Admin</Link>
              </Menu.Item>
              {/* <SubMenu className="submenu" title="PENDAFTARAN"></SubMenu> */}
              <Menu.Item className="menuMenu" key="patient" icon={<UserAddOutlined />}>
              <Link className="linkstyle" to="/patient">Patient</Link>
              </Menu.Item>
              <Menu.Item className="menuMenu" key="oldPatient" icon={<UserAddOutlined />}>
              <Link className="linkstyle" to="/oldPatient">Old Patient</Link>
              </Menu.Item>
              <Menu.Item className="menuMenu" key="doctor" icon={<UserSwitchOutlined />}>
              <Link className="linkstyle" to="/doctor">Doctor</Link>
              </Menu.Item>
              <Menu.Item className="menuMenu" key="visited" icon={<UploadOutlined />}>
                <Link className="linkstyle" to="/visited">Visited</Link>
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className="site-layout">
            <Header className="site-layout-background">
              {React.createElement(this.state.collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
                className: 'trigger',
                onClick: this.toggle,
              })}
              <a className="logout" href="/" icon={<UserOutlined />}><b>Logout</b></a>
            </Header>
            <Content className="content">{this.props.children}</Content>
          </Layout>
        </Layout>
        );
    }
}
