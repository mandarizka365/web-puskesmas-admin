import { createSelector } from 'reselect';

const templateState = (state: any) => state.TemplateState;

export const sideIsOpenSelector = () =>
  createSelector(templateState, state => state.siderIsOpen);

export const modalDoctorSelector = () =>
  createSelector(templateState, state => state.showModalDoctor);

export const modalVisitedSelector = () =>
  createSelector(templateState, state => state.showModalVisited);

export const modalUserSelector = () =>
  createSelector(templateState, state => state.showModalUser);

export const modalPatientSelector = () =>
  createSelector(templateState, state => state.showModalPatient);

export const modalOldSelector = () =>
  createSelector(templateState, state => state.showModalOld);
export const modalVisiSelector = () =>
  createSelector(templateState, state => state.showModalVisi);