import * as ActionRegis from '../Store/RegisAction';
import * as SelectorRegis from '../Selector/RegisSelector';

import { call, put, select, takeLatest } from 'redux-saga/effects';

import axios from 'axios';
import history from "../../App/History"

function* submitRegisProcces() {
  try {
    const name = yield select(
      SelectorRegis.formNameSelector()
    );
    const username = yield select(
      SelectorRegis.formUsernameSelector()
    );
    const password = yield select(SelectorRegis.formPasswordSelector());

    yield call(axios.post, 
      `${process.env.REACT_APP_LOGIN_URL}/Login/AddLogin`, {
      name,
      username,
      password,
    });

    yield put(ActionRegis.fetchRegisListFinished());
    history.push("/");
  } catch (error) {
    console.log(error);
  }
}

export function* submitRegisAction() {
  yield takeLatest('SUBMIT_REGIS_REQUESTED', submitRegisProcces);
}

