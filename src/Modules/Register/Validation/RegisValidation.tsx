export default function RegisValidation(values) {
  const errors: any = {};
  if (!values.name) {
    errors.name = 'Name Required!';
  } else if (values.name.length < 3) {
    errors.name = 'Minimal name 3 characters';
  };
  if (!values.username) {
      errors.username = 'Username Required!';
    } else if (values.username.length < 5) {
      errors.username = 'Minimal Username 5 characters';
    }
    if (!values.password) {
      errors.password = 'Password Required!';
    } else if (values.password.length < 5) {
      errors.password = 'Minimal Password 5 characters';
    }
    return errors;
}
