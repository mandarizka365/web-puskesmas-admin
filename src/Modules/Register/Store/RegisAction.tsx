import { Action } from 'redux';

// dr template action

export interface IFetchRegis extends Action {
  data: object;
}

export interface IModalAction extends Action {
  typeAction: string;
}

export interface ISetId extends Action {
  id: string;
}

export interface ISetDetail extends Action {
  data: object;
}

// fetchRegisListRequested functin utk nembak API

export function fetchRegisListRequested() {
  return {
    type: 'FETCH_REGIS_LIST_REQUESTED',
  };
}

export function updateRegisRequested() {
  return {
    type: 'UPDATE_REGIS_REQUESTED',
  };
}


export function setRegisId(id: string): ISetId {
  return {
    type: 'SET_REGIS_ID',
    id,
  };
}

export function deleteRegisRequested(id: string): ISetId {
  return {
    type: 'DELETE_REGIS_REQUESTED',
    id,
  };
}

export function setRegisDetail(data: object): ISetDetail {
  return {
    type: 'SET_REGIS_DETAIL',
    data,
  };
}

export function submitRegisRequested() {
  return {
    type: 'SUBMIT_REGIS_REQUESTED',
  };
}

export function fetchRegisListFinished(){
  return {
    type: 'FETCH_REGIS_LIST_FINISHED',
  };
}

export function changeModalAction(typeAction: string): IModalAction {
  return {
    type: 'CHANGE_MODAL_REGIS_ACTION',
    typeAction,
  };
}
