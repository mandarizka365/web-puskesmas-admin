import { createSelector } from 'reselect';

const regisState = (state: any) => state.RegisState;
const regisFormState = (state: any) => state.form.regisForm.values;

export const listSelector = () =>
  createSelector(regisState, state => state.list);
export const idSelector = () =>
  createSelector(regisState, state => state.selectedIdRegis);
export const selectedUserSelector = () =>
  createSelector(regisState, state => state.selectedRegis);
export const modalActionSelector = () =>
  createSelector(regisState, state => state.modalAction);

//SELECTOR FORM
export const formNameSelector = () =>
  createSelector(regisFormState, state => state.name);
export const formUsernameSelector = () =>
  createSelector(regisFormState, state => state.username);
export const formPasswordSelector = () =>
  createSelector(regisFormState, state => state.password);

