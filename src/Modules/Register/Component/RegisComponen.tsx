import { Button, Col, Form, Layout, Row } from 'antd';
import { Field, reduxForm } from 'redux-form';

import { InputPassword } from '../../../Assets/Components/CinputPassword';
import { InputText } from '../../../Assets/Components/CInputText';
import React from 'react';
import images from '../../App/Images'
import validate from '../Validation/RegisValidation';

const { Content } = Layout;


function RegisComponen(props) {
  const {
    handleOnSubmit,
    invalid,
  } = props;
  return (
    <Layout style={{height:"100vh"}}>
      <Content className="content1">
      <Row>
      
      <Col span={12}>
      <div className="divcolomdua">
      <Form onFinish={handleOnSubmit} layout="horizontal">
      <Field
          name="name"
          component={InputText}
          label="Name"
          placeholder="Input name here"
        />
        
        <Field
          name="username"
          component={InputText}
          label="Username"
          placeholder="Input username here"
        />


        <Field
          name="password"
          component={InputPassword}
          label="Password"
          placeholder="Input password here"
        />

        <Form.Item>
          <Button type="primary" htmlType="submit" disabled={invalid}>
            Submit
          </Button>
        </Form.Item>
      </Form>
      </div>

      </Col>
      <Col span={12}>
        <img src={images.gambarsatu} alt="doctor"  className="pict1"/>
      </Col>
    </Row>
      </Content>
    </Layout>
      
  );
}

export default reduxForm({
  form: 'regisForm',
  validate,
  enableReinitialize: true,
})(RegisComponen); //HOC
