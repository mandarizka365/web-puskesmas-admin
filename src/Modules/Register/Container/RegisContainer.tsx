import * as RegisAction from '../Store/RegisAction';
import * as TemplateAction from '../../Template/Store/TemplateAction';

import { bindActionCreators, compose } from 'redux';

import React from 'react';
import RegisComponen from '../Component/RegisComponen';
import { connect } from 'react-redux';

function RegisContainer(props) {
  const { actionRegis } = props;
  const handleOnSubmit = () => {
    actionRegis.submitRegisRequested();
  };

  const initialValues: any = {};
  return (
    <RegisComponen
      {...props}
      initialValues={initialValues}
      handleOnSubmit={handleOnSubmit}
    />
  );
}

const mapDispatchToProps = (dispatch: any) => ({
  actionRegis: bindActionCreators(RegisAction, dispatch),
  actionTemplate: bindActionCreators(TemplateAction, dispatch),
});
const withConnect = connect(null, mapDispatchToProps);
export default compose(withConnect)(RegisContainer);
