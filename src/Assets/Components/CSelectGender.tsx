import { Form, Select } from 'antd';

import React from 'react';

const {Option} = Select;

interface ISelectGender {
  select;
  input;
  meta;
  label;
  data;
}
export const SelectGender = (props : ISelectGender) => {
  const {
  input,
  meta: { touched, error },
  label,
  data,
}=props;

  const onChange = e => input.onChange(e);

  const optionData = data.map(data => {
    const { id, name } = data;
    return (
      <Option value={id}>{name}</Option>
    );
  });
  return (
    <Form.Item
      validateStatus={touched && error !== undefined ? 'error' : ''}
      help={touched && error !== undefined ? error : ''}
      label={label}
    >
      <Select onChange={onChange} value={input.value}>
        {optionData}
      </Select>
    </Form.Item>
  );
  };