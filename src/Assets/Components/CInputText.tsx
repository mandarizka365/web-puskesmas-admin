import { DatePicker, Form, Input, Space } from 'antd';

import React from 'react';

interface IInputText {
  input;
  meta;
  placeholder;
  label?;
}

interface IDatePicker {
  input;
  meta;
  moment;
  label?;
}
export const InputText = ({
  input,
  meta: { touched, error },
  label,
  placeholder,
}: IInputText) => {
  return (
    <Form.Item
      validateStatus={touched && error !== undefined ? 'error' : ''}
      help={touched && error !== undefined ? error : ''}
      label={label}
    >
      <Input placeholder={placeholder} {...input} />
    </Form.Item>
  );
};

export const DatePickers = ({
  meta: { touched, error },
  input,
}: IDatePicker) => {
  return (
    <Form.Item
    // validateStatus={touched && error !== undefined ? 'error' : ''}
    // help={touched && error !== undefined ? 'error' : ''}
    style={{ marginBottom: 0}}
    >
      <Space
        style={{ borderRadius: 14, marginBottom: 10, float:'right'}}
        direction='vertical'
        size={14}>
        
        <DatePicker
        onChange={value => input.onChange(value)}
        format="DD-MMMM-YYYY"
        />
        
      </Space>
    </Form.Item>
  );
};
// {...input} karna ada byk jenis input kita pakai yg ada
//validateStatus biar difrom muncul eror merah
