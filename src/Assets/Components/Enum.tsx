export const Gender = [
    {
        id : "Male",
        name : "Male"
    },
    {
        id : "Female",
        name : "Female"
    }
];

export const Poli = [
    {
        id : "Umum",
        name : "Umum"
    },
    {
        id : "Anak",
        name : "Anak"
    },
    {
        id : "Gigi",
        name : "Gigi"
    },
    {
        id : "Imunisasi",
        name : "Imunisasi"
    },
    {
        id : "THT",
        name : "THT"
    },
    {
        id : "KB",
        name : "KB"
    },
    {
        id : "KIA",
        name : "KIA"
    },
    {
        id : "KonsultasiGizi",
        name : "KonsultasiGizi"
    }
];

export const Buy = [
    {
        id : "BPJS",
        name : "BPJS"
    },
    {
        id : "Bayar",
        name : "Bayar"
    }
];