import { Form, Input } from 'antd';

import React from 'react';

interface IInputPassword {
  input;
  meta;
  placeholder;
  label?;
}

export const InputPassword = ({
  input,
  meta: { touched, error },
  label,
  placeholder,
}: IInputPassword) => {
  return (
    <Form.Item
      validateStatus={touched && error !== undefined ? 'error' : ''}
      help={touched && error !== undefined ? error : ''}
      label={label}
    >
      <Input.Password placeholder={placeholder} {...input} />
    </Form.Item>
  );
};